#Corporate Site for Provalores, Grupo Promerica partner

Grupo Promerica is a Financial group based in Central America, Provalores Stock exchange, part of this group, needed to merge into corporate style using a flexible technology which pertmit them build and deploy a robust website in the shortest possible time with a security Guarantee in its development.

Provalores Nicaragua - https://provalores.com.ni

#Development Actors:
1 ROR Developer

1 Web Developer / Web Designer

#Tecnologias utilizadas
Ruby

Ruby on Rails Framework

Refinery CMS

Postgresql

#Auth
Devise Auth (Devise Gem) - https://github.com/plataformatec/devise

#Style
Corporative style, according Financial Group

#Features
As Web site is build with Refinerycms it inherit its properties such as:

Supports latest versions of Rails

Modular and extendable

Design flexibility (The most important feature for our company, since we build all ours designs from scratch)

Fast to develop in (Since is build with ruby on rails)

Easy to use


#Proceso de desarrollo
Conceptualizacion

Art Design

HTML - CSS Layouts Modeling 

Implementing

Deployment

#Design:
Template / Layout design

Maquetacion Mobile First

CSS animations and transitions